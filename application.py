import tkinter as tk
from numberama import Numberama


class Application:

    def __init__(self, root):
        """Initiates GUI class for game.

        Args:
            root (tk.Tk): Main window of the game.
        """
        self.root = root
        self.nbr = Numberama()
        self.number_1 = None
        self.number_2 = None
        self.mainframe = tk.Frame(self.root)
        self.draw_buttons()
        self.draw_numbers()
        self.mainframe.pack(expand=True)

    def draw_numbers(self):
        """Draws frame with all the numbers.
        """
        self.numbers_frame = tk.Frame(self.mainframe)
        for row, line in enumerate(self.nbr.data):
            for column, number in enumerate(line):
                if number == 0:
                    number = " "
                elif number is None:
                    self.numbers_frame.pack(side="bottom", fill="x")
                    return
                number_label = tk.Label(self.numbers_frame,
                                        text=str(number))
                number_label.coordinates = [row + 1, column + 1]
                number_label.number = number
                number_label.bind("<Button-1>", self.number_click)
                number_label.config(relief="raised")
                number_label.grid(row=row+1, column=column)
        self.numbers_frame.pack(side="bottom", fill="x")

    def draw_buttons(self):
        """Draws frame for buttons and buttons also.
        """
        self.buttons_frame = tk.Frame(self.root)
        self.buttons_row = tk.Frame(self.buttons_frame)
        self.double_button = tk.Button(self.buttons_row, 
                                       text="double",
                                       command=self.double_numbers)
        self.restart_button = tk.Button(self.buttons_row, 
                                        text="restart",
                                        command=self.restart_game)
        self.double_button.pack(side="left")
        self.restart_button.pack(side="right")
        self.buttons_row.pack()
        self.buttons_frame.pack(side="top")

    def number_click(self, event):
        """After clicking on a first number, it marks it as
            clicked. If number is clicked before, it checks
            if numbers are pair and then refreshes frame.

        Args:
            event (tk.Event): Action that was performed on a widget.
        """
        widget = event.widget
        if self.number_1 is None:
            self.number_1 = widget.coordinates
            widget.config(relief="sunken")
            return None
        elif self.number_1 == widget.coordinates:
            self.number_1 = None
            widget.config(relief="raised")
            return None
        elif self.number_2 is None:
            self.number_2 = widget.coordinates
            if self.nbr.null_number_pair(self.number_1, self.number_2):
                self.show_victory()
            else:
                self.number_1, self.number_2 = None, None
                self.refresh_window()

    def restart_game(self):
        """Resets numbers.
        """
        self.nbr.data = self.nbr.read_data()
        self.refresh_window()

    def double_numbers(self):
        self.nbr.double_numbers()
        self.refresh_window()

    def refresh_window(self):
        """Redraws numbers frame with new values.
        """
        self.numbers_frame.destroy()
        self.draw_numbers()

    def show_victory(self):
        """Shows victory text, when no number is left.
        """
        self.numbers_frame.destroy()
        self.numbers_frame = tk.Frame(self.mainframe)
        victory_label = tk.Label(self.numbers_frame,
                                 text="Victory!")
        victory_label.pack(fill="x")
        self.numbers_frame.pack(side="bottom", fill="x")
