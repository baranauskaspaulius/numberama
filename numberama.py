import numpy as np
class Numberama:
    """Game class that deals with numbers.
    """
    def __init__(self):
        self.data = self.read_data()
        self.row_count = len(self.data)
        self.column_count = len(self.data[0])

    def __str__(self):
        """Prints all the data that is currently available.

        Returns:
            str: all the numbers formatted as a string.
        """
        columns_nums = list(range(1, 10))
        columns_nums = [str(i) for i in columns_nums]
        output = " ".join(columns_nums)
        output = "  | {output}\n{separator}".format(output=output,
                                                    separator=21 * "-")
        for n, line in enumerate(self.data):
            for i, number in enumerate(line):
                line = list(line)
                if number == 0:
                    line[i] = " "
                elif number is None:
                    line[i] = "x"
                else:
                    line[i] = str(number)
            line = " ".join(line)
            output = "{previous}\n {n}| {new}".format(previous=output,
                                                      new=line,
                                                      n=n + 1)
        return output

    def read_data(self):
        """Reads starting numbers from a csv file.

        Returns:
            List: List of starting numbers.
        """
        with open("starting_list.csv") as f:
            data = f.read()
            data = np.array(np.mat(data))
        return data

    def check_pair(self,
                   num_1: list,
                   num_2: list):
        """Checks if sum of num_1 and num_2 is equal to 10 or integers are equal.

        Args:
            num_1 (list): First integer for comparisson.
            num_2 (list): Second integer for comparisson.

        Returns:
            Bool: True if pair sum is equal to 10,
                or numbers in pair are the same.
        """
        if num_1 == num_2:
            raise ValueError("Indices must not point to the same number")
        number_1 = self.data[num_1[0]-1, num_1[1]-1]
        number_2 = self.data[num_2[0]-1, num_2[1]-1]
        if num_1 == 0:
            raise ValueError("Selected first number is empty")
        if num_2 == 0:
            raise ValueError("Selected second number is empty")
        if not self.check_if_neighbours(num_1, num_2):
            return False
        if (number_1 == number_2) | \
                (number_1 + number_2 == 10):
            return True
        else:
            return False

    def check_if_neighbours(self,
                            num_1: list,
                            num_2: list):
        """Takes coordinates of two numbers and check if they are neigbours
            horizontally or vertically.

        Args:
            num_1(list): coordinates of the first number
            num_2(list): coordinates of the second number

        Returns:
            bool: Flag if numbers are neighbours horizontally or vertically.
        """
        if (num_1[1] == num_2[1]) & \
                (num_1[0] != num_2[0]):
            if self.check_vertical(num_1, num_2):
                return True
        elif num_2[0] - num_1[0] > 1:
            return False
        elif self.check_horizontal(num_1, num_2):
                return True
        else:
            return False

    def check_vertical(self,
                       num_1: list,
                       num_2: list):
        """Checks if numbers are neighbours vertically.

        Args:
            num_1(list): coordinates of the first number
            num_2(list): coordinates of the second number

        Returns:
            bool: Flag if numbers are neighbours vertically.
        """
        for i in range(0, num_2[0] - num_1[0] + 1):
            row = num_1[0] + i + 1
            if row == num_2[0]:
                return True
            if self.data[row - 1, num_1[1] - 1] == 0:
                continue
            elif self.data[row - 1, num_1[1] - 1] != 0:
                return False

    def check_horizontal(self,
                         num_1: list,
                         num_2: list):
        """Takes two numbers coordinates and then checks if these numbers are
            neighbours horizontally.

        Args:
            num_1(list): Coordinates of the first number.
            num_2(list): Coordinates of the second number.

        Returns:
            bool: Flag if numbers are neighbours horizontally.
        """
        if num_2[0] - num_1[0] > 1:
            return False
        i, j = num_1
        k, l = num_2
        while i <= k:
            while True:
                j += 1
                if j > 9:
                    j = 1
                    i += 1
                if (i == k) & \
                        (j == l):
                    return True
                elif self.data[i - 1, j - 1] != 0:
                    return False

    def null_number_pair(self,
                         num_1,
                         num_2):
        """Removes two numbers when they are neighbours and match
            predefined conditions.

        Args:
            num_1 (list): coordinates of the first number.
            num_2: (list): coordinates of the second number.
        """
        if num_1[0] > num_2[0]:
            num_1, num_2 = num_2, num_1
        elif (num_1[1] > num_2[1]) &\
                (num_1[0] == num_2[0]):
            num_1, num_2 = num_2, num_1
        if self.check_pair(num_1, num_2):
            self.data[num_1[0] - 1,
                      num_1[1] - 1] = 0

            self.data[num_2[0] - 1,
                      num_2[1] - 1] = 0
        self.remove_empty_rows()
        return self.check_victory()

    def remove_empty_rows(self):
        """Clears rows that have no numbers.
        """
        row_nums = []
        for i, row in enumerate(self.data):
            if len(row) == 9:
                if all(row == [0] * 9):
                    row_nums.append(i)
        if len(row_nums) > 0:
            self.data = np.delete(self.data, row_nums, 0)

    def double_numbers(self):
        """Repeats the numbers that are still left at the end of
            current left matrix.
        """
        self.remove_empty_rows()
        new_data = self.data.copy()
        last_line = new_data[len(new_data) - 1]
        new_data = np.delete(new_data, len(new_data) - 1, 0)
        for line in self.data:
            for number in line:
                if number == 0:
                    continue
                elif number is None:
                    break
                for i, place in enumerate(last_line):
                    if place is None:
                        last_line[i] = number
                        break
                if None not in last_line:
                    new_data = np.append(new_data, [last_line.copy()], 0)
                    last_line = np.array([None] * 9)
        if None in last_line:
            new_data = np.append(new_data, [last_line.copy()], 0)
        self.data = new_data.copy()

    def check_victory(self):
        """Checks if there is any number left and returns bool
            that shows if there is any.

        Returns:
            bool: shows if any number is left.
        """

        if len(self.data) == 0:
            return True
        elif len(self.data) == 1:
            if list(range(1, 10)) in self.data[0]:
                return False
            else:
                return True
        else:
            return False
