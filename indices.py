def split_input(text_input: str):
    """Splits input text into list of parameters

    Args:
        text_input (str): Text, that user has entered in console when prompted
            for indices.

    Returns:
        list: List of int indices.
    """
    if len(text_input) < 3:
        raise FormatException("Not enough indices or no separator.")
    if "," in text_input:
        indices = text_input.split(",")
    elif " " in text_input:
        indices = text_input.split(" ")
    elif ";" in text_input:
        indices = text_input.split(";")
    else:
        raise FormatException("Indices have to be separated by colon, semicolon (',', ';') or space")

    if len(indices) > 2:
        raise FormatException("Too many indices")

    try:
        indices = [int(i) for i in indices]
    except ValueError:
        raise FormatException("Indices have to be integer")

    return indices

def get_indexes():
    """Prompts for indices, splits text by colon, semicolon or space.

    Returns:
        list: list with indices for number [row, column]
    """
    while True:
        text_input = input("Enter number's row number and column number: ")
        try:
            indices = split_input(text_input)
            return indices
        except FormatException as e:
            print(e)
